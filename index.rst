Documentation for yc-lexisnexis-scraper
=======================================

This small Python module is intended to be a demonstration of how to use `PhantomJS <http://phantomjs.org/>`__ and `Selenium WebDriver <http://www.seleniumhq.org/>`__ in Python to scrape websites.
This code is not intended for use in anyway that contravenes LexisNexis Academic `Terms of Use <http://www.lexisnexis.com/terms/>`__.

Disclaimer: We accept no liability for the content of this code, or for the consequences of any actions taken on the basis of the information provided.

This code is licensed under `Apache License, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0>`__.

.. toctree::

LexisNexis Scraper
------------------

.. automodule:: lexisnexis
   :members:
   :special-members:
